package solution;

import java.util.HashMap;

/**
 *
 * @author topie
 */
public class Compiler {

    private final HashMap<String, Integer> opcodes;

    public Compiler() {
        opcodes = new HashMap<>();
        opcodes.put("print", 0x02);
        opcodes.put("inc", 0x40);
        opcodes.put("dec", 0x48);
        opcodes.put("push", 0x50);
        opcodes.put("pop", 0x58);
        opcodes.put("jnz", 0x70);
        opcodes.put("int", 0xcc);
    }

    public byte[] compile(byte[] src) {
        return this.compile(new String(src));
    }

    public byte[] compile(String src) {

        System.out.println("Compiling program...");

        String[] lines = src.split("\n");
        byte[] dst = new byte[lines.length * 2];

        int i = 0;
        for (String line : lines) {

            // Cut comments
            if (line.contains(";")) {
                line = line.substring(0, line.indexOf(";"));

            }

            int opcode, value;

            String[] args = line.split("\\s+");

            if (args.length == 0 || args[0].length() == 0) {
                continue;
            }
            
            if (opcodes.containsKey(args[0])) {
                opcode = opcodes.get(args[0]);
                String a = args[1];
                // Convert registers to indices
                if (a.matches("^e[abcd]x$")) {
                    value = a.charAt(1) - 97; // eax -> 0, ebx -> 1, ...
                    opcode++; // turn opcode into register variant
                } else {
                    value = Integer.parseInt(args[1]);
                }
            } else {
                opcode = 0;
                value = 0;
            }

            dst[i++] = (byte) opcode;
            dst[i++] = (byte) value;
        }

        return dst;
    }
}
