package solution;

/**
 * TX00CF83-3006 course assignment solution 21: Facade pattern
 *
 * @author topie
 */
public class Solution {

    public static void main(String[] args) {
        ComputerFacade computer = new ComputerFacade();
        computer.start();
    }

}
