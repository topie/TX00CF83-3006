package solution;

import java.util.Arrays;

public class Memory {

    private byte[] data = new byte[65536];
    public final long BOOT_ADDRESS = 2048; // everything up to this is stack

    public void load(long position, byte[] data) {
        System.out.println("Loading " + data.length + " bytes into memory");
        System.arraycopy(data, 0, this.data, (int) position, data.length);
    }

    public byte read(long lba) {
        return this.data[(int) lba];
    }

    public byte[] read(long lba, int size) {
        return Arrays.copyOfRange(this.data, (int) lba, (int) (lba + size));
    }

    public void write(long lba, byte data) {
        this.data[(int) lba] = data;
    }

    public void write(long lba, byte[] data) {
        System.arraycopy(data, (int) lba,
                this.data, 0, (int) (lba + data.length));
    }
}
