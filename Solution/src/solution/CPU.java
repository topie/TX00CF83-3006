package solution;

import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CPU {

    private int stackPointer;
    private int programCounter;
    private byte result; // Previous computation
    private final byte[] register;
    private boolean running;
    private final Thread core;

    private final Memory memory;

    @SuppressWarnings("empty-statement")
    public CPU(Memory memory) {
        System.out.println("Processor powering up...");
        this.memory = memory;
        this.register = new byte[4]; // eax, ebx, ecx, edx

        this.result = 0;
        this.stackPointer = 0;
        this.programCounter = 0;

        this.core = new Thread(() -> {
            while (this.tick());
        });

        this.running = true;
        this.core.start();
    }

    public void freeze() {
        this.running = false;
        System.out.println("(!) Processor halted");
    }

    public void jump(long position) {
        this.programCounter = (int) position / 2;
    }

    public void execute() {
        this.running = true;
        System.out.println("(!) Processor running");
    }

    public boolean execute(byte instruction, byte arg) {

        int opcode = Byte.toUnsignedInt(instruction);
        byte value = arg;

        // Odd numbers mean arguments are register values instead of literals
        if ((opcode & 0x01) == 0x01) {
            value = this.register[arg];
            opcode--; // normalize to an even number
        }

        switch (opcode) {
            case 0x02: // PRINT (as a character to console)
                System.out.print((value == 13)
                        ? '\n'
                        : Character.toString((char) value));
                break;

            case 0x40: // INC (increment by 1)
                this.result = ++this.register[arg];
                break;

            case 0x48: // DEC (decrement by 1)
                this.result = --this.register[arg];
                break;

            case 0x50: // PUSH (to stack)
                this.result = this.push(value);
                break;

            case 0x58: // POP (from stack)
                this.result = this.pop();
                this.register[arg] = this.result;
                break;

            case 0x70: // JNZ (jump if not zero)
                if (this.result != 0) {
                    this.programCounter += value - 1;
                }
                break;

            case 0xcc: // INT (interrupt)
                System.out.println("Program terminated");
                this.freeze();
                return false;

            default: // NOOP (no operation)
                System.out.println("(no-op)");
                break;
        }

        return true;
    }

    private byte push(byte value) {
        this.memory.write(this.stackPointer++, value);
        return value;
    }

    private byte pop() {
        return this.memory.read(--this.stackPointer);
    }

    @SuppressWarnings("empty-statement")
    private boolean tick() {
        // Wait if frozen
        while (!this.running);

        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            Logger.getLogger(CPU.class.getName()).log(Level.SEVERE, null, ex);
        }

        int i = 2 * this.programCounter++;

        return this.execute(
                this.memory.read(i), // opcode
                this.memory.read(i + 1)); // arg
    }
}
