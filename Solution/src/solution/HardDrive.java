package solution;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HardDrive {

    private byte[] data;

    public final int BOOT_SECTOR;
    public final int SECTOR_SIZE;

    public HardDrive() {
        System.out.println("HDD is spooling up...");

        // Read drive contents from file
        try {
            this.data = Files.readAllBytes(Paths.get("src/solution/hdd_contents.txt"));

        } catch (IOException ex) {
            System.out.println("Failed to read the HDD.");
            Logger.getLogger(HardDrive.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.BOOT_SECTOR = 100;
        this.SECTOR_SIZE = this.data.length - this.BOOT_SECTOR;
    }

    public byte[] read(long lba, int size) {
        System.out.println("Reading " + size + " bytes from HDD...");
        return Arrays.copyOfRange(this.data, (int) lba, (int) (lba + size));
    }
}
