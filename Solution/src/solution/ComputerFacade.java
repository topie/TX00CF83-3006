package solution;

class ComputerFacade {
    private CPU processor;
    private Memory ram;
    private HardDrive hd;

    public ComputerFacade() {
        this.ram = new Memory();
        this.processor = new CPU(this.ram);
        this.hd = new HardDrive();
    }

    public void start() {
        Compiler c = new Compiler();
        processor.freeze();
        ram.load(ram.BOOT_ADDRESS,
                c.compile(
                        hd.read(hd.BOOT_SECTOR, hd.SECTOR_SIZE)));
        processor.jump(ram.BOOT_ADDRESS);
        processor.execute();
    }
}
